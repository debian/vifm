vifm (0.13-1) unstable; urgency=medium

  * New upstream release (Closes: #1084114)
  * Remove d/vim-vifm.yaml (deprecated)
  * Remove d/.git-dpm (deprecated)
  * Remove d/patches/perl-interpreter-fix.patch
    Use #!/usr/bin/env perl for better portability
  * Update d/upstream/metadata
    - Add some useful fields
  * d/control:
    - Update package description
    - Update Standards-Version to 4.7.0
    - Add Perl to package dependencies
    - Add myself to Maintainer field
    - New maintainer (Closes: #1088857)
  * d/copyright
    - Resolve 'lrc' discrepancies
    - Add myself to Copyright field
    - Switch URLs from HTTP to HTTPS
    - Fix Lintian (inconsistent-appstream-metadata-license) warning
  * d/watch
    - Fix incorrect/deprecated lines (fix uscan warn)
  * d/tests/control
    - Add some simple tests
  * Update d/rules (actualization)
    - Add security flags to LDFLAGS for enhanced protection
  * Remove not actual d/upstream/signing-key.asc
    - Fix Lintian (debian-watch-could-verify-download) warning
  * Switch 'vim-addon-manager' to 'dh-sequence-vim-addon' (Closes: #953980)
    - Add ${vim-addon:Depends} to package dependencies
    - Actualization of d/install

 -- Kirill Rekhov <krekhov.dev@gmail.com>  Sat, 08 Feb 2025 19:34:55 +0300

vifm (0.12.1-1) unstable; urgency=medium

  * New upstream release.
  * d/watch: Fix GitHub.
  * Bump standards version to 4.6.2.
  * d/copyright:
    - Bump my copyright years.
    - Fix for new upstream version.
  * Build-Depends: libncursesw5-dev -> libncurses-dev.
  * Don't remove debian/vifm/usr/share/vifm/vim-doc (Closes: #1024875).

 -- Ondřej Nový <onovy@debian.org>  Tue, 28 Feb 2023 09:10:56 +0100

vifm (0.12-1) unstable; urgency=medium

  * d/watch: Change to GitHub.
  * Drop patches which are not needed or applied upstream:
    - drop-vifm-media-osx.patch
    - fix-hurd.patch
    - fix-kfreebsd.patch
    - gcc-10-fix.patch
  * d/rules:
    - Set TERM=xterm
    - Don't remove /usr/share/vifm/vifmrc-osx (doesn't exists in new release)

 -- Ondřej Nový <onovy@debian.org>  Wed, 13 Oct 2021 15:18:06 +0200

vifm (0.10.1-4) unstable; urgency=medium

  * Fix GCC-10 (Closes: #957910)
  * Bump standards version to 4.5.0.
  * Bump debhelper compat level to 13.
  * Set Rules-Requires-Root: no.

 -- Ondřej Nový <onovy@debian.org>  Thu, 20 Aug 2020 08:44:41 +0200

vifm (0.10.1-3) unstable; urgency=medium

  * Fix FTBFS on kfreebsd.

 -- Ondřej Nový <onovy@debian.org>  Sat, 24 Aug 2019 18:51:30 +0200

vifm (0.10.1-2) unstable; urgency=medium

  * Fix FTBFS on Hurd.

 -- Ondřej Nový <onovy@debian.org>  Sun, 11 Aug 2019 13:35:56 +0200

vifm (0.10.1-1) unstable; urgency=medium

  * New upstream release.
  * Use debhelper-compat instead of debian/compat.
  * Bump debhelper compat level to 12.
  * Bump standards version to 4.4.0 (no changes).
  * d/patches/drop-vifm-media-osx.patch: Don't install vifm-media-osx script

 -- Ondřej Nový <onovy@debian.org>  Fri, 02 Aug 2019 10:47:46 +0200

vifm (0.10-1) unstable; urgency=medium

  * New upstream release

 -- Ondřej Nový <onovy@debian.org>  Fri, 23 Nov 2018 13:15:04 +0100

vifm (0.9.1-2) unstable; urgency=medium

  * d/changelog: Remove trailing newline
  * Use /usr/bin/perl as Perl interpreter
  * Bump standards version to 4.2.1 (no changes)
  * d/watch: Use https protocol
  * Add upstream metadata
  * Install vifm-help.txt (LP: #1774852)

 -- Ondřej Nový <onovy@debian.org>  Sat, 29 Sep 2018 18:21:02 +0200

vifm (0.9.1-1) unstable; urgency=medium

  * New upstream release
  * Use dh_missing --fail-missing
  * Bump Standards-Version to 4.1.3 (no changes needed)
  * d/copyright
    - Change upstream author email
    - Use https for format
  * Bump debhelper compat level to 11
  * d/control: Change Vcs-* to Salsa

 -- Ondřej Nový <onovy@debian.org>  Fri, 09 Feb 2018 14:56:56 +0100

vifm (0.9-1) unstable; urgency=medium

  * New upstream release
  * Bump debhelper version to 10
  * Check upstream tarball signature

 -- Ondřej Nový <onovy@debian.org>  Tue, 20 Jun 2017 16:58:33 +0200

vifm (0.8.2-1) unstable; urgency=medium

  * New upstream release
  * Uploading to unstable
  * Migrated to git-dpm
  * Removed d/p/filetype_unit_test_fix.patch (applied upstream)

 -- Ondřej Nový <onovy@debian.org>  Thu, 21 Jul 2016 21:40:14 +0200

vifm (0.8.2~beta-1) experimental; urgency=medium

  * New upstream release
  * d/{control,copyright}: Use my @debian.org email address
  * Removed all patches, applied upstream
  * Upstream url changed to https protocol
  * Added filetype_unit_test_fix.patch

 -- Ondřej Nový <onovy@debian.org>  Mon, 04 Jul 2016 13:36:26 +0200

vifm (0.8.1a-1) unstable; urgency=medium

  * New maintainer (Closes: #824002)
  * wrap-and-sort -t -a
  * Added Vcs-* fields pointing to the collab-maint repository
  * Fixed vim plugin (Closes: #770195)
  * Removed redundant files from /usr/share/vifm

 -- Ondřej Nový <novy@ondrej.org>  Wed, 11 May 2016 23:20:03 +0200

vifm (0.8.1a-0.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Removed override_dh_auto_test which prevented to see errors from tests
  * Added patches from upstream:
    - use_CFLAGS_passed_to_configure_in_tests.patch
    - fix_for_architectures_on_which_char_is_unsigned.patch
    - use_return_value_of_chdir_in_misc_tests.patch
  * Enabled all PIE hardening

 -- Ondřej Nový <novy@ondrej.org>  Tue, 03 May 2016 23:37:52 +0200

vifm (0.8.1a-0.1) unstable; urgency=medium

  * Non-maintainer upload.
  * New upstream release (Closes: #812797, #810630)
  * Removed 0001-fix-building-without-terminal.patch
    (Applied upstream)
  * Removed 0002-Fix-buffer-overflow-in-tests-to-fix-building-on-arm.patch
    (Applied upstream)
  * Standards-Version is 3.9.8 now (no change)
  * d/copyright:
    - Fixed for new release
    - Added myself for Debian part
  * Removed superfluous doc files (COPYING, INSTALL and ChangeLog)
  * Removed d/menu, .desktop file is installed instead
  * Added simple Debian test
  * Enabled all but PIE hardening
  * Added disable-build-timestamp option to perform reproducible build

 -- Ondřej Nový <novy@ondrej.org>  Sat, 09 Apr 2016 19:27:44 +0200

vifm (0.7.8-3) unstable; urgency=medium

  * Added patch to fix building on arm-archs
    Thanks to xaizek for providing another fix very quickly!

 -- Hendrik Jaeger <deb@henk.geekmail.org>  Sun, 26 Oct 2014 11:41:34 +0100

vifm (0.7.8-2) unstable; urgency=medium

  * Removed non-functional 'make tests' fixup in debian/rules
  * Added patch to fix building without terminal
    Thanks to xaizek for providing the patch so quickly

 -- Hendrik Jaeger <deb@henk.geekmail.org>  Sun, 26 Oct 2014 01:42:39 +0200

vifm (0.7.8-1) unstable; urgency=medium

  * [54b44c8] New Upstream version 0.7.8 (Upload to unstable)
  * [ffc0b9c] Changed Standards-Version from 3.9.4 to 3.9.6 in debian/control
  * [8f0860c] Changed path in debian/install to reflect upstream changes
  * [feaa914] Changed Homepage in debian/control to http://vifm.info/
  * [08a6d6f] Changed Source and Upstream-Contact in debian/copyright
  * [f0e76b8] Changed Copyright time period in debian/copyright
  * [085c125] Added missing Copyright holder to debian/copyright
  * [75c7b88] Added missing license information in debian/copyright
  * [f21acf8] Added support for -beta releases to debian/watch
  * [d8d0029] Added override in debian/rules to make tests work
  * [a73f042] Added locale setting in debian/rules to fix manpage generation
  * [da21229] Added dh_autoreconf to fix build for new archs (Closes: #748283)
  * [5a97013] Added dynamic support for X11
  * [157f3d9] Added explicit --without-gtk to configure call in debian/rules
  * [f927fb2] Added vim as Build-Dependency for helptags generation
  * [2b3754c] Removed debian/clean, obsoleted by upstream.
  * [32d0e01] Removed obsolete patch from debian/patches/series
  * [9560a73] Removed obsolete patchfiles

 -- Hendrik Jaeger <deb@henk.geekmail.org>  Sat, 25 Oct 2014 20:39:52 +0200

vifm (0.7.4a-1~exp2) experimental; urgency=low

  * Removed unnecessary build-dependency on 'quilt'
  * Added options to configure call to avoid extra dependencies:
    - --without-X11
    - --without-gtk

 -- Hendrik Jaeger <deb@henk.geekmail.org>  Mon, 26 Nov 2012 23:14:45 +0100

vifm (0.7.4a-1~exp1) experimental; urgency=low

  * New maintainer (Closes: #682456)
  * New upstream release (Closes: #577280):
    - vifm-pause script was renamed (Closes: #592562)
    - proper utf8-support has been included (Closes: #526253, #495805)
    - possible buffer overflow fixed, debian-dir cleaned up (Closes: #520458)
    - dirs with spaces are now handled correctly (Closes: #483474)
  * Added patches to fix tests
    - tests/Makefile.inc: add include to find curses.h
  * Added debian/clean to delete build-leftovers
  * Added debian/compat to state compatibility with debhelper 9
  * Added homepage to package description
  * Added debian/install to install the vifm-plugin for vim where it can be
    used with vim-addon-manager
  * Added debian/menu to have vifm show up in autogenerated menus
  * Added debian/source/format with "3.0 (quilt)"
  * Added needed files to vim-vifm.yaml
  * Changed debian policy version to 3.9.4
  * Changed package description text to be more informative
  * Changed debian/copyright to be machine parsable (DEP-5)
  * Changed debian/rules to use dh for everything
  * Changed debian/watch to correct format and file extension
  * Changed dependency to libncursesw5-dev

 -- Hendrik Jaeger <deb@henk.geekmail.org>  Sun, 23 Sep 2012 02:10:58 +0200

vifm (0.4-1) unstable; urgency=low

  * new upstream release with UTF-8 support (Closes: #495805)

 -- Edelhard Becker <edelhard@debian.org>  Thu, 11 Sep 2008 22:35:37 +0200

vifm (0.3a-3) unstable; urgency=low

  * make vifm build-depend on automake and libtool (Closes: #477735)

 -- Edelhard Becker <edelhard@debian.org>  Fri, 25 Apr 2008 21:55:56 +0200

vifm (0.3a-2) unstable; urgency=low

  * okay, okay, with vim-addon-manager we don't need a postinst with helpztags
    any more ...

 -- Edelhard Becker <edelhard@debian.org>  Tue, 22 Apr 2008 17:35:55 +0200

vifm (0.3a-1) unstable; urgency=low

  * new upstream release
  * debian/rules: call autogen.sh, fixed clean target
  * fixed debian/watch file (Closes: #450233)
  * added patches from CVS
  * bump standards to 3.7.3 (no changes)
  * depend on vim|vim-tiny, install helpfile in /usr/share/vim/addons/doc,
    vifm.vim in /usr/share/vim/addons/plugin and use vim-addon-manager for
    local and system-wide installation (Closes: #443479); do not install both
    as conffiles anymore
  * look for DEB_BUILD_OPTIONS=nostrip (Closes: #438245)

 -- Edelhard Becker <edelhard@debian.org>  Mon, 21 Apr 2008 15:40:09 +0200

vifm (0.3-2) unstable; urgency=low

  * ACK NMU, thanks Blars (Closes: #320118)

 -- Edelhard Becker <edelhard@debian.org>  Tue,  9 Aug 2005 15:15:09 +0200

vifm (0.3-1.1) unstable; urgency=low

  * NMU durring BSP
  * add build-depend on automake1.7 (Closes: #320118)

 -- Blars Blarson <blarson@debian.org>  Fri,  5 Aug 2005 19:37:55 +0000

vifm (0.3-1) unstable; urgency=low

  * New upstream release
  * removed CC=cc from debian/rules (broke with libtool --tag)
  * remove CVS dirs from upstream tarball

 -- Edelhard Becker <edelhard@debian.org>  Tue, 19 Jul 2005 15:30:01 +0200

vifm (0.2a-2) unstable; urgency=low

  * Renamed pauseme to vifm-pauseme (making a diversion or putting it into a
    seperate package just seems to be too much effort for a 8-line shell
    script, IMHO; Closes: #274782: file conflicts in vifm tkdesk).

 -- Edelhard Becker <edelhard@debian.org>  Mon,  4 Oct 2004 11:35:25 +0200

vifm (0.2a-1) unstable; urgency=low

  * New upstream release (thanks Jeroen van Wolffelaar for the hint)
  * changed Maintainer to my debian account
  * updated standards to 3.6.1 (no changes needed)
  * removed CVS directories from source

 -- Edelhard Becker <edelhard@debian.org>  Wed, 29 Sep 2004 18:00:36 +0200

vifm (0.2-2) unstable; urgency=low

  * updated watch file

 -- Edelhard Becker <edelhard@debian.org>  Wed, 29 Sep 2004 16:09:30 +0200

vifm (0.2-1) unstable; urgency=low

  * New upstream release
  * install helpfile vifm.txt and vim plugin vifm.vim in /etc/vim
  * patched config.c to reflect package file structure

 -- Edelhard Becker <becker@edelhard.de>  Wed, 23 Apr 2003 19:07:16 +0200

vifm (0.1-4) unstable; urgency=low

  * reworked package to be built without debhelper
  * first upload (Closes: bug#182420).

 -- Edelhard Becker <becker@edelhard.de>  Wed, 26 Feb 2003 17:59:03 +0100

vifm (0.1-3) unstable; urgency=low

  * deleted postinst from debian dir (was just empty after removing the
    old-style /usr/doc link)

 -- Edelhard Becker <becker@edelhard.de>  Wed, 13 Nov 2002 01:03:58 +0100

vifm (0.1-2) unstable; urgency=low

  * changed the colors in ui.c to be a bit nicer and consistent with (at least
    my and therefore Debian default) $LS_COLORS

 -- Edelhard Becker <becker@edelhard.de>  Thu, 29 Aug 2002 19:40:31 +0200

vifm (0.1-1) unstable; urgency=low

  * Initial Release.

 -- Edelhard Becker <becker@edelhard.de>  Tue, 27 Aug 2002 19:19:28 +0200
